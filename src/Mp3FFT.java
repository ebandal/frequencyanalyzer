

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

public class Mp3FFT implements Runnable {
    File file;
    FrequencyHandler frequencyHandler;
    static float SAMPLE_RATE;

    
    public Mp3FFT(File file, FrequencyHandler frequencyHandler) {
		this.file = file; // new File(filename); // "Sevila-David Russel.mp3");
    	this.frequencyHandler = frequencyHandler;
    }
 
    private double[] convertToDouble1(byte[] byteBuf, int nBytesRead, int bytesPerSample, boolean bigEndian) {
    	double[] targetChannel;
    	
		if (bytesPerSample == 1) {
			targetChannel = new double[nBytesRead];
			
			for (int i=0; i < nBytesRead; i++) {
				targetChannel[i] = byteBuf[i] / 128.0; // normalise to range [-1, 1];
			}
		} else if (bytesPerSample == 2) { // 16 bit
			targetChannel = new double[nBytesRead/2];
			for (int i=0; i < nBytesRead/2; i++) {
				int sample;
				byte lobyte;
				byte hibyte;
				if (!bigEndian) {
					lobyte = byteBuf[i*2];
					hibyte = byteBuf[i*2 + 1];
				} else {
					lobyte = byteBuf[i*2 + 1];
					hibyte = byteBuf[i*2];
				}
				sample = hibyte << 8 | lobyte & 0xFF;
				targetChannel[i] = sample / 32768.0;// normalise to range [-1, 1];
			}
		} else { // bytesPerSample == 3, i.e. 24 bit
			targetChannel = new double[nBytesRead/3];
			for (int i=0; i < nBytesRead/3; i++) {
				int sample;
				byte lobyte;
				byte midbyte;
				byte hibyte;
				if (!bigEndian) {
					lobyte = byteBuf[i*3];
					midbyte = byteBuf[i*3 + 1];
					hibyte = byteBuf[i*3 + 2];
				} else {
					lobyte = byteBuf[i*3 + 2];
					midbyte = byteBuf[i*3 + 1];
					hibyte = byteBuf[i*3];
				}
				sample = hibyte << 16 | (midbyte & 0xFF) << 8 | lobyte & 0xFF;
				targetChannel[i] = sample / 8388606.0; // normalise to range [-1, 1]
			}
		}
		return targetChannel;
		
    }
		
    private double[][] convertToDouble2(byte[] byteBuf, int nBytesRead, int bytesPerSample, boolean bigEndian) {
    	double[][] targetChannel = new double[2][];
    	
		if (bytesPerSample == 1) {
			targetChannel[0] = new double[nBytesRead/2];
			targetChannel[1] = new double[nBytesRead/2];
			
			for (int i=0, idx=0; i < nBytesRead; i++,idx++) {
				targetChannel[0][idx] = byteBuf[i] / 128.0; // normalise to range [-1, 1];
				targetChannel[1][idx] = byteBuf[i+1] / 128.0; // normalise to range [-1, 1];
			}
		} else if (bytesPerSample == 2) { // 16 bit
			targetChannel[0] = new double[nBytesRead/4];
			targetChannel[1] = new double[nBytesRead/4]; // new double[nBytesRead * 2];
			for (int i=0, idx=0; i < nBytesRead; i+=4, idx++) {
				int sample;
				byte lobyte;
				byte hibyte;
				if (!bigEndian) {
					lobyte = byteBuf[i];
					hibyte = byteBuf[i+1];
				} else {
					lobyte = byteBuf[i+1];
					hibyte = byteBuf[i];
				}
				sample = hibyte << 8 | lobyte & 0xFF;
				targetChannel[0][idx] = sample / 32768.0;// normalise to range [-1, 1];
				
				if (!bigEndian) {
					lobyte = byteBuf[i+2];
					hibyte = byteBuf[i+3];
				} else {
					lobyte = byteBuf[i+3];
					hibyte = byteBuf[i+2];
				}
				sample = hibyte << 8 | lobyte & 0xFF;
				targetChannel[1][idx] = sample / 32768.0;// normalise to range [-1, 1];
			}
		} else { // bytesPerSample == 3, i.e. 24 bit
			targetChannel[0] = new double[nBytesRead/6];
			targetChannel[1] = new double[nBytesRead/6];
			for (int i=0,idx=0; i < nBytesRead; i+=6,idx++) {
				int sample;
				byte lobyte;
				byte midbyte;
				byte hibyte;
				if (!bigEndian) {
					lobyte = byteBuf[i];
					midbyte = byteBuf[i+1];
					hibyte = byteBuf[i+2];
				} else {
					lobyte = byteBuf[i+2];
					midbyte = byteBuf[i+1];
					hibyte = byteBuf[i];
				}
				sample = hibyte << 16 | (midbyte & 0xFF) << 8 | lobyte & 0xFF;
				targetChannel[0][idx] = sample / 8388606.0; // normalise to range [-1, 1]
				if (!bigEndian) {
					lobyte = byteBuf[i+3];
					midbyte = byteBuf[i+4];
					hibyte = byteBuf[i+5];
				} else {
					lobyte = byteBuf[i+5];
					midbyte = byteBuf[i+4];
					hibyte = byteBuf[i+3];
				}
				sample = hibyte << 16 | (midbyte & 0xFF) << 8 | lobyte & 0xFF;
				targetChannel[1][idx] = sample / 8388606.0; // normalise to range [-1, 1]
			}
		}
		return targetChannel;
		
    }

    private double[] transform(double[] input) {   

	    double[] tempConversion = null;
	    FastFourierTransformer transformer = new FastFourierTransformer(DftNormalization.STANDARD);
	    try {           
	        Complex[] complx = transformer.transform(input, TransformType.FORWARD);
		    tempConversion = new double[complx.length];
		    
	        for (int i = 0; i < complx.length; i++) {
	            double rr = (complx[i].getReal());
	            double ri = (complx[i].getImaginary());

	            tempConversion[i] = Math.sqrt((rr * rr) + (ri * ri));
	        }
	    } catch (IllegalArgumentException e) {
	        System.out.println(e);
	    }
	    return tempConversion;
	}
	
	public void run() {
		ExecutorService pool = Executors.newFixedThreadPool(2);
         
		try {
			AudioInputStream mp3In = AudioSystem.getAudioInputStream(file);
		    AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(file);  
		    long frameLen = fileFormat.getFrameLength();
		    
			AudioInputStream pcmIn = null;
			AudioFormat mp3Format = mp3In.getFormat();
			int bitsPerSample = mp3Format.getSampleSizeInBits();
			bitsPerSample = (bitsPerSample==AudioSystem.NOT_SPECIFIED?16:bitsPerSample);
			boolean isBigEndian = mp3Format.isBigEndian();
			int nChannel = mp3Format.getChannels();
			float samplingRate =  mp3Format.getSampleRate();
			int nFrameSize = mp3Format.getFrameSize();
            nFrameSize = nFrameSize==AudioSystem.NOT_SPECIFIED?mp3Format.getChannels()*2:nFrameSize;
            
            // samplingRate	44100		44100			16*1024
            // frameRate	38.28125	43.06640625		16
            // frameSize	1152		1024			1024
            SAMPLE_RATE = mp3Format.getSampleRate();
			AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
														mp3Format.getSampleRate(),
														16, 
														mp3Format.getChannels(), // 1,
														mp3Format.getChannels()*2, // 2,
														mp3Format.getFrameRate(),
			                                            false);
			pcmIn = AudioSystem.getAudioInputStream(decodedFormat, mp3In);
			// long frameLen = pcmIn.getFrameLength();
			
			int nBufSize = 4608;
			byte[] byteBuf = new byte[nBufSize]; 
			int nBytesRead = 0;
			boolean stopped = false;
			// 8192, 16384, 32768 frame버퍼가 클수록 주파수 영역이 골고루 해석 된다.
			// 32768 정도면  모든 건반에 대한 주파수 해석이 가능.
			// double[] frameDouble = new double[32768];	
			
			final DataLine.Info info = new DataLine.Info(SourceDataLine.class, decodedFormat);
	        final SourceDataLine soundLine = (SourceDataLine) AudioSystem.getLine(info);
	        soundLine.open(decodedFormat, 4608*2*2 * 2); // 1152 * 38.28125 * 2Ch * 2Bytes
	        soundLine.start();
	        long before = Instant.now().toEpochMilli();

			while (!stopped) {
				nBytesRead = pcmIn.read(byteBuf, 0, byteBuf.length);
				if (nBytesRead==-1) { stopped=true; break; }
				
				
				
				double[][] readDouble = convertToDouble2(byteBuf, nBytesRead, bitsPerSample/8, false);
				// So if your sample rate, Fs is say 44.1 kHz and your FFT size, N is 1024, then the FFT output bins are at:

				//	0:   0 * 44100 / 1024 =     0.0 Hz
				//  1:   1 * 44100 / 1024 =    43.1 Hz
				//  2:   2 * 44100 / 1024 =    86.1 Hz
				//  3:   3 * 44100 / 1024 =   129.2 Hz
				//  4: ...
				//  5: ...
				//     ...
				//  511: 511 * 44100 / 1024 = 22006.9 Hz
				// 	Note that for a real input signal (imaginary parts all zero) the second half of the FFT (bins from N / 2 + 1 to N - 1) 
				//  contain no useful additional information (they have complex conjugate symmetry with the first N / 2 - 1 bins). 
				//  The last useful bin (for practical aplications) is at N / 2 - 1, which corresponds to 22006.9 Hz in the above example. 
				//  The bin at N / 2 represents energy at the Nyquist frequency, i.e. Fs / 2 ( = 22050 Hz in this example), 
				//  but this is in general not of any practical use, since anti-aliasing filters will typically attenuate any signals at and above Fs / 2.
				
				int validLength = Math.max(32768, nextPower2(readDouble[0].length));
				double[] frameDouble = new double[validLength * 2];
				Arrays.fill(frameDouble, (double)0);
				System.arraycopy(readDouble[0], 0, frameDouble, 0, readDouble[0].length);
				double[] result = transform(frameDouble);
				double[] notes = new double[88];
				if (result != null) {
					pool.execute(new Runnable() {
						public void run() {
							for (int i=0; i<result.length/2; i++) {
								float freq = i*SAMPLE_RATE/result.length;	// 440Hz에서 주파수가 나와야 하는데, 660Hz에서 나옴. 왜???? 
								float dKeyNum = (float)(Math.log(freq/440.0)/Math.log(2)*12 + 49);
								int nKeyNum = Math.round(dKeyNum);
								if (nKeyNum > 0 && nKeyNum < 89)
									if (notes[nKeyNum-1] < result[i])
										notes[nKeyNum-1] = result[i];
							}
							frequencyHandler.onFrequencies(notes);
						}
					});
				}
				
				int availLen = soundLine.available();

				soundLine.write(byteBuf, 0, nBytesRead);

				try {
					if (availLen > 4608*3) { 
						// System.out.println("" + Instant.now().toEpochMilli() + ":no sleep:" + availLen);
						// Thread.sleep(24);
					} else if (availLen > 4608*2) { 
						// System.out.println("" + Instant.now().toEpochMilli() + ":30msec sleep:" + availLen);
						Thread.sleep(30);
					} else if (availLen > 4608*1) {
						// System.out.println("" + Instant.now().toEpochMilli() + ":40msec sleep:" + availLen);
						Thread.sleep(40);
					} else {
						// System.out.println("" + Instant.now().toEpochMilli() + ":50 sleep:" + availLen);
						Thread.sleep(50);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
//				long now = Instant.now().toEpochMilli();
//				before = now;

			}
			soundLine.drain();
			soundLine.stop();
			soundLine.close();
			
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			pool.shutdown();
		}
	}
	
	private static int nextPower2(int n) {
		int m = (int)(Math.log(n)/Math.log(2.0));
		int nPower2 = (int)Math.pow(2, m);
		
		if (nPower2 == n) 
			return n;
		else 
			return (int)Math.pow(2, m+1);
	}
}
