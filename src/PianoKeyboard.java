import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileFilter;
import javax.sound.midi.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.TreeMap;
import java.util.Vector;

public class PianoKeyboard extends JPanel {
	private static final long serialVersionUID = 1L;

	final int ON = 0, OFF = 1;
    final Color jfcBlue = new Color(204, 204, 255);
    final Color pink = new Color(255, 175, 175);

    Map<Integer, Key> allKeys = new TreeMap<>();
    JTable table;
    Piano piano;
    boolean record;
    Track track;
    long startTime;

    FrequencyHandler handler;
    double[] spec;
    double maxSpec = 100.0;

    public PianoKeyboard() {
    	
        setLayout(new GridLayout(2, 1));

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        EmptyBorder eb = new EmptyBorder(5,5,5,5);
        BevelBorder bb = new BevelBorder(BevelBorder.LOWERED);
        CompoundBorder cb = new CompoundBorder(eb,bb);
        p.setBorder(new CompoundBorder(cb,eb));
        JPanel pp = new JPanel(new BorderLayout());
        pp.setBorder(new EmptyBorder(10,20,10,5));
        pp.add(piano = new Piano(handler));
        p.add(pp);

        StackedBar bar = new StackedBar();
		bar.setSize(907,200);

		add(p);
		add(bar);
        
		// Mp3FFT fft = new Mp3FFT("Twinkle-twinkle-little-star-mozart.mp3", handler);
		// new Thread(fft).start();
	}

    private void addMp3(File file) {
    	handler = new FrequencyHandler() {
			@Override
			public void onFrequencies(double[] frequencies) {
				spec = frequencies;
				OptionalDouble maxVal = Arrays.stream(frequencies).max();
				if (maxVal.isPresent()) {
					if (maxVal.getAsDouble() > maxSpec) {
						// maxSpec = (maxVal.getAsDouble() + maxSpec*4)/5;
						maxSpec = maxVal.getAsDouble();
					}
				}
				
				double limit = Math.max(maxVal.orElse(0)*0.7, 50);
				// double limit = maxVal.orElse(0)*0.8;
				
				for (int i=0; i < spec.length; i++) {
					if (spec[i] > limit) {
						allKeys.get(i+1).noteState=ON;
					} else {
						allKeys.get(i+1).noteState=OFF;
					}
				}
				repaint();
        	}
        };
        Mp3FFT fft = new Mp3FFT(file, handler);
        new Thread(fft).start(); 
    }
    
    
    /**
     * Black and white keys or notes on the piano.
     */
    class Key extends Rectangle {
		private static final long serialVersionUID = 1L;
		int noteState = OFF;
        int kNum;
        double freq;
        boolean isBlack;
        public Key(int x, int y, int width, int height, int num, boolean isBlack) {
            super(x, y, width, height);
            kNum = num;
            freq = Math.pow(2, ((double)kNum-49)/12)*440;
            this.isBlack = isBlack;
        }
        public boolean isNoteOn() {
            return noteState == ON;
        }
        public void on() {
            setNoteState(ON);
        }
        public void off() {
            setNoteState(OFF);
        }
        public void setNoteState(int state) {
            noteState = state;
        }
        public void print() {
        	System.out.println("Num="+kNum+",Freq="+freq);
        }
    } // End class Key


    class Piano extends JPanel {

		private static final long serialVersionUID = 1L;
        final int kw = 16, kh = 80;

        public Piano(FrequencyHandler handler) {
            setLayout(new BorderLayout());
            setPreferredSize(new Dimension(42*kw, kh+1));
            int transpose = 1;  
            int whiteIDs[] = { 0, 2, 3, 5, 7, 8, 10 }; 
          
            for (int i = 0, x = 0; i < 8; i++) {
                for (int j = 0; j < 7; j++, x += kw) {
                	if (i==7 && j==3) break;
                    int keyNum = i * 12 + whiteIDs[j] + transpose;
                    Key key = new Key(x, 0, kw, kh, keyNum, false);
                    allKeys.put(keyNum, key);
                }
            }
            for (int i = 0, x = 0; i < 8; i++) {
                int keyNum = i * 12 + transpose;
                Key key = new Key((x += kw)-6, 0, kw/2+4, kh/2+10, keyNum+1, true);
                allKeys.put(keyNum+1, key);
                if (i==7) break;
                x += kw;
                key = new Key((x += kw)-6, 0, kw/2+4, kh/2+10, keyNum+4, true);
                allKeys.put(keyNum+4, key);
                key = new Key((x += kw)-6, 0, kw/2+4, kh/2+10, keyNum+6, true);
                allKeys.put(keyNum+6, key);
                x += kw;
                key = new Key((x += kw)-6, 0, kw/2+4, kh/2+10, keyNum+9, true);
                allKeys.put(keyNum+9, key);
                key = new Key((x += kw)-6, 0, kw/2+4, kh/2+10, keyNum+11, true);
                allKeys.put(keyNum+11, key);
            }
        }

        public void paint(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            Dimension d = getSize();

            g2.setBackground(getBackground());
            g2.clearRect(0, 0, d.width, d.height);

            g2.setColor(Color.white);
            g2.fillRect(0, 0, 52*kw, kh);
            allKeys.values().stream().filter(key -> !key.isBlack).forEach(key -> {
            											if (key.noteState==ON) {
            							                    g2.setColor(record ? pink : jfcBlue);
            							                    g2.fill(key);
            											}
            							                g2.setColor(Color.black);
            							                g2.draw(key);
            										});
            allKeys.values().stream().filter(key -> key.isBlack).forEach(key -> {
										                if (key.noteState==ON) {
										                    g2.setColor(record ? pink : jfcBlue);
										                    g2.fill(key);
										                    g2.setColor(Color.black);
										                    g2.draw(key);
										                } else {
										                    g2.setColor(Color.black);
										                    g2.fill(key);
										                }
													});
        }
    } // End class Piano

    class StackedBar extends JPanel {
		public void paint(Graphics g){
			if (spec == null) return;
			
			g.setColor(Color.black);
			g.fillRect(32,5,831,118);

			g.setColor(Color.green);
			for(int i=0; i < 88; i++) {
				g.fillRect((int)(32+i*9.45),5+118-(int)(spec[i]*118/maxSpec),((i*9.45%1)>0.5)?10:9,(int)(spec[i]*118/maxSpec));
			}
		}
    }
	
    private static JMenuBar makeMenu(ActionListener listner) {
    	JMenuBar menuBar = new JMenuBar();
    	JMenu menu = new JMenu("File");
    	menuBar.add(menu);
    	JMenuItem menuItem = new JMenuItem("Open mp3");
    	menuItem.addActionListener(listner);
    	menu.add(menuItem);
    	
    	return menuBar;
    }
    
    public static void main(String args[]) {
        final PianoKeyboard keyboard = new PianoKeyboard();
        JFrame f = new JFrame("Piano Frequency Analyzer");
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });
        
        int w = 907;
        int h = 305;
        f.setPreferredSize(new Dimension(w, h));
    	f.setJMenuBar(makeMenu(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JMenuItem source = (JMenuItem)(e.getSource());
		        switch(source.getText()) {
		        case "Open mp3":
		        	final JFileChooser fc = new JFileChooser();
		            fc.setFileFilter(new Mp3Filter());
		            fc.setAcceptAllFileFilterUsed(false);
		            int returnVal = fc.showOpenDialog(f);
		            if (returnVal == JFileChooser.APPROVE_OPTION) {
		                File file = fc.getSelectedFile();
		                //This is where a real application would open the file.
		                keyboard.addMp3(file);
		            }
		        	break;
		        }
			}
    	}));
   
        f.add(keyboard);
        f.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        f.setLocation(screenSize.width/2 - w/2, screenSize.height/2 - h/2);
        f.setVisible(true);
        f.setResizable(false);
    }
}

class Mp3Filter extends FileFilter {
    //Accept all directories and mp3 files.
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
 
        if (f.getName().matches(".+\\.mp3")) {
        	return true;
        } else {
        	return false;
        }
    }
 
    //The description of this filter
    public String getDescription() {
        return "mp3";
    }
}
